FROM ubuntu:20.04

RUN apt update \
    && apt-get install -y --no-install-recommends \
    sudo \
    screen \
	tmate \
    bash \
    curl \
    git \
	unzip \
	wget \
	&& apt-get update \
	&& apt-get upgrade -y \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m kunesa && \
    adduser kunesa sudo && \
    sudo usermod -a -G sudo kunesa

COPY . /

ADD /downloads/ngrok
RUN chmod +x /ssh.sh
RUN ./ssh.sh
RUN chmod +x /run.sh
RUN chmod +x /timeout.sh
RUN screen -dmS run ./run.sh && sleep5
RUN ls -a && ./timeout.sh